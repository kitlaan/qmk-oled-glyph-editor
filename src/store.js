import Vue from 'vue';

export const store = Vue.observable({
  showInvert: true,

  nextKey: 1,
  glyphs: {},

  imgkeys: [],

  uniqueness: {},
});

function hashimgdata(imgdata) {
  return imgdata.join();
}

export const mutations = {
  addGlyph(ord, imgdata) {
    const key = store.nextKey;
    store.nextKey++;

    Vue.set(store.glyphs, key, {
      ord: ord,
      imgdata: [],
      selected: false
    });

    for (let ix = 0; ix < imgdata.length; ix++) {
      Vue.set(store.glyphs[key].imgdata, ix, imgdata[ix] ? 1 : 0);
    }

    const hash = hashimgdata(store.glyphs[key].imgdata);
    if (hash in store.uniqueness) {
      store.uniqueness[hash]++;
    }
    else {
      Vue.set(store.uniqueness, hash, 1);
    }

    return key;
  },
  deleteGlyph(key) {
    if (key in store.glyphs) {
      const hash = hashimgdata(store.glyphs[key].imgdata);
      if (hash in store.uniqueness) {
        store.uniqueness[hash]--;
      }

      Vue.delete(store.glyphs, key);
    }
  },
  editGlyph(key, ord, imgdata) {
    if (key in store.glyphs) {
      store.glyphs[key].ord = ord;

      var hash = hashimgdata(store.glyphs[key].imgdata);
      if (hash in store.uniqueness) {
        store.uniqueness[hash]--;
      }

      for (let ix = 0; ix < imgdata.length; ix++) {
        Vue.set(store.glyphs[key].imgdata, ix, imgdata[ix] ? 1 : 0);
      }

      hash = hashimgdata(store.glyphs[key].imgdata);
      if (hash in store.uniqueness) {
        store.uniqueness[hash]++;
      }
      else {
        Vue.set(store.uniqueness, hash, 1);
      }
    }
  },
  toggleGlyph(key) {
    if (key in store.glyphs) {
      store.glyphs[key].selected = !store.glyphs[key].selected;
    }
  },
  isGlyphUnique(key) {
    if (key in store.glyphs) {
      const hash = hashimgdata(store.glyphs[key].imgdata);
      if (hash in store.uniqueness) {
        return store.uniqueness[hash] <= 1;
      }
    }
    return true;
  },
  toggleInvert() {
    store.showInvert = !store.showInvert;
  },
  deleteSelectedGlyphs() {
    for (var key in store.glyphs) {
      if (store.glyphs[key].selected) {
        const hash = hashimgdata(store.glyphs[key].imgdata);
        if (hash in store.uniqueness) {
          store.uniqueness[hash]--;
        }

        Vue.delete(store.glyphs, key);
      }
    }
  },
  deselectAllGlyphs() {
    for (var key in store.glyphs) {
      if (store.glyphs[key].selected) {
        store.glyphs[key].selected = false;
      }
    }
  },
  addImage(imgkeys) {
    store.imgkeys.push(imgkeys);
  },
  reset() {
    store.nextKey = 1;
    for (let key in store.glyphs) {
      Vue.delete(store.glyphs, key);
    }
    store.imgkeys = []; // TODO: fixme reactive?
    for (let key in store.uniqueness) {
      Vue.delete(store.uniqueness, key);
    }
  },
};

// TODO: follow https://austincooper.dev/2019/08/09/vue-observable-state-store/
// to do getters, actions, mutations
