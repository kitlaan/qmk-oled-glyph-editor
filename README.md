# QMK OLED Glyph Editor

A client-side browser app to edit/create OLED source code
for use in the QMK Firmware.

The SSD1306-based monochrome OLEDs used on keyboards tend to
be 128x32 or 128x64 in size. The QMK firmware bitpacks 8-pixels
vertically into each byte, with the LSB being the top-pixel.

To save flash space, a "font" is defined to allow mapping from
characters to a grid of pixels. To make coding easier, the
default `OLED_FONT_HEIGHT` is 8, to match the bitpacking.
(The code itself doesn't seem to work properly with other
values.)

The default "font glyph" size is 6x8 (width x height) pixels.
That means the byte representation of a single glyis is 6-bytes.

For example, `0x01 0x00 0x00 0x00 0x00 0x80` would represent:
```
X.....
......
......
......
......
......
......
.....X
```

## Official Website(s)

* https://qmkoled.mostlyuseful.tech
* https://kitlaan.gitlab.io/qmk-oled-glyph-editor
* https://gitlab.com/kitlaan/qmk-oled-glyph-editor

## Development

### Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
